FROM maven:3.9.6-amazoncorretto-17

WORKDIR /code

COPY . .

RUN mvn clean install

CMD mvn spring-boot:run