# Workouts API

A RESTful API written using the Spring Boot framework.

The API is available at https://api.workout-odyssey.com

The Swagger documentation is available [here](https://api.workout-odyssey.com/v1/docs/swagger-ui/index.html)

## Features
- **Easy workout management**: The API provides a simple way to manage your workouts from all your devices. With endpoints to get, update, create, and delete workouts, you can easily track your fitness progress and make changes as needed.
- **Secure authentication**: The API includes endpoints for user authentication, allowing you to create new accounts and sign in to existing ones. With JWT security, you can be sure that your data is protected and only accessible to authorized users.
- **Customizable workout records**: The API allows you to retrieve workout records by type, such as running, walking, or cycling. This gives you the flexibility to track your fitness progress in the way that works best for you.
- **Efficient preview check**: With the preview endpoint, you can quickly check if a workout already exists before creating a new one. This can help you avoid duplicate entries and keep your data organized.
- **Well-documented**: The API is built on the widely used Spring Boot framework and adheres to the OpenAPI specification, making it easy to integrate with your existing systems. Additionally, the API includes comprehensive documentation, making it easy to get started and scale as needed.

## Local development
The Workouts API requires Java 17

The app can be run in a Docker container along with a MySQL database:
- Clone the repo and `cd` into the root directory
- Run `sh ./scripts/start.sh`

This should build the `workouts-api` image (if run for the first time) and run its container in a detached mode alongside a MySQL database container which uses `workouts-db` volume to persist data in the host filesystem. The service is accessible on port `8080`

Other available scripts:
- `stop.sh` - stops and removes the workouts API and MySQL containers
- `teardown.sh` - stops and removes the workouts API and MySQL containers and their associated data including volumes (this will remove workouts database from host filesystem!)

If any changes are made to the application code, the service can be stopped (`stop.sh`) and run again (`start.sh`) to update the containers.

In case of running the service outside of docker container (e.g. from and IDE) a `.env` file containing environment variables must be placed in the root directory. The required environment variables can be found in `.env.example` file

The MySQL database can be dumped into an SQL file using `mysqldump -h <hostname> -u <username> -p<password> --hex-blob --set-gtid-purged=OFF <dbname> > <dumpfile>.sql` and restored with `mysql -h <hostname> -u <username> -p<password> <dbname> < <dumpfile>.sql`

### E2E Tests
A reproducible environment can be run with `startTest.sh` script which runs the Workouts API service a MySQL database populated with test data from `dbTestData` directory. The database is not persisted and is recreated every time the script is run. This service should be stopped with `testCleanup.sh`

## Endpoints

- [Status](#Status)
- [API Authentication](#API-Authentication)
    - [Create an account](#Create-an-account)
    - [Login](#Login)
- [Workouts](#Workouts)
    - [Get workout by id](#Get-workout-by-id)
    - [Get workout geolocation](#Get-workout-geolocation)
    - [Get the dashboard](#Get-the-dashboard)
    - [Get all workouts](#Get-all-workouts)
    - [Get best results](#Get-best-results)
    - [Get workout preview](#Get-workout-preview)
    - [Add new workout](#Add-new-workout)
    - [Edit workout](#Edit-workout)
    - [Delete workout](#Delete-workout)
- [Labels](#Labels)
    - [Get labels](#Get-labels)
- [Users](#Users)
    - [Get user details](#Get-user-details)
    - [Delete a user](#Delete-a-user)


## Status

**`GET /status`**

Returns the status of the API. Example response:

```json
{
  "name": "Workouts API",
  "status": "UP"
}
```

Status `UP` indicates that the API is running as expected.

No response or any other response indicates that the API is not functioning correctly.


## API Authentication

Most endpoints require authentication. To obtain an access token you need to create an account and log in.

The endpoints that require authentication expect a bearer token sent in the `Authorization` header.

Example:

`Authorization: Bearer YOUR_TOKEN`

### Create an account

**`POST /v1/auth/register`**

**Parameters**

The request body needs to be in JSON format.

| Name       | Type   | In   | Required | Description         |
|------------| ------ | ---- | -------- |---------------------|
| `username` | string | body | Yes      | Your email address. |
| `password` | string | body | Yes      | Password.           |
| `name`     | string | body | Yes      | Your name.          |

**Status codes**

| Status code     | Description                                                                                                       |
|-----------------|-------------------------------------------------------------------------------------------------------------------|
| 201 Created     | Indicates that the client has been registered successfully.                                                       |
| 400 Bad Request | Indicates that the parameters provided are invalid or a user with this email address has already been registered. |

Example request body:

```json
{
    "username": "jon@example.com",
    "name": "Jon",
    "password": "Password"
}
```

Example response:

```json
{
    "email": "jon@example.com",
    "name": "Jon",
    "verified": false,
    "createdAt": "2023-02-19T15:03:33.542+00:00",
    "updatedAt": "2023-02-19T15:03:33.543+00:00",
    "lastLogin": null,
    "loginCount": 0
}
```

### Login

**`POST /v1/auth/login`**

**Parameters**

The request body needs to be in JSON format.

| Name       | Type   | In   | Required | Description         |
|------------| ------ | ---- | -------- |---------------------|
| `username` | string | body | Yes      | Your email address. |
| `password` | string | body | Yes      | Password.           |

**Status codes**

| Status code     | Description                                                                                                       |
|-----------------|-------------------------------------------------------------------------------------------------------------------|
| 200 OK          | Indicates a successful response.                                                                                                        |
| 401 Unauthorized | Indicates that the parameters provided are invalid. |

Example request body:

```json
{
    "username": "jon@example.com",
    "password": "Password"
}
```

Example response:

```json
{
    "accessToken": "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJhQGVtYWlsLmNvbSIsImlhdCI6MTY3NjgxOTI0NiwiZXhwIjoxNjc5NDExMjQ2fQ.6RrauP9zW4sSa60uBTP-hre1joR4zNkNuBQ34neWI3FdMN2oukG7Ch8cFHc2L1Ae",
    "refreshToken": "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJhQGVtYWlsLmNvbSIsImlhdCI6MTY3NjgxOTI0NiwiZXhwIjoxNjc5NDExMjQ2fQ.6RrauP9zW4sSa60uBTP-hre1joR4zNkNuBQ34neWI3FdMN2oukG7Ch8cFHc2L1Ae",
    "tokenType": "Bearer"
}
```

## Workouts

### Get workout by id

Returns a single workout of a given type.

**`GET /v1/workouts/:id`**

**Parameters**

| Name            | Type    | In     | Required | Description                                                                |
|-----------------|---------|--------|----------|----------------------------------------------------------------------------|
| `Authorization` | string  | header | Yes      | Specifies the bearer token of the API client.                              |
| `id`            | integer | path   | Yes      | Specifies the id of the workout to be returned.                            |
| `geolocation`   | boolean | query  | No       | Specifies whether the returned workout data should include the geolocation |

**Status codes**

| Status code | Description |
|-----------------|-----------------------------------------------------|
| 200 OK | Indicates a successful response. |
| 400 Bad Request | Indicates that the parameters provided are invalid. |

Example response:

```json
{
    "distance": 10.0,
    "duration": 3005.0,
    "timestamp": "2020-06-25 23:00:00",
    "notes": null,
    "type": "running",
    "id": 1000,
    "pace": 300.5,
    "speed": 11.980033277870216,
    "label": null,
    "trajectory": null,
    "hasTrajectory": true,
    "utcOffset": 0
}
```

### Get workout geolocation

Returns geolocation data for a workout with a given id.

**`GET /v1/workouts/:id/geolocation`**

**Parameters**

| Name            | Type    | In     | Required | Description                                                                 |
|-----------------|---------|--------|----------|-----------------------------------------------------------------------------|
| `Authorization` | string  | header | Yes      | Specifies the bearer token of the API client.                               |
| `id`            | integer | path   | Yes      | Specifies the id of the workout for which the geolocation data is returned. |

**Status codes**

| Status code | Description |
|-----------------|-----------------------------------------------------|
| 200 OK | Indicates a successful response. |
| 400 Bad Request | Indicates that the parameters provided are invalid. |

Example response:

```json
{
    "workoutId": 132,
    "trajectory": {
        "type": "Feature",
        "properties": null,
        "geometry": {
            "type": "LineString",
            "coordinates": [
                [
                  0.0378378,
                  51.5939045
                ],
                [
                  0.0378389,
                  51.5938874
                ]
            ]
        }
    }
}
```

### Get the dashboard

Returns yearly and monthly metrics for a given workout type.

**`GET /v1/workouts/dashboard/:type`**

**Parameters**

| Name            | Type   | In     | Required | Description                                                                 |
| --------------- |--------| ------ | -------- |-----------------------------------------------------------------------------|
| `Authorization` | string | header | Yes      | Specifies the bearer token of the API client.                               |
| `type`     | string | path  | Yes      | Specifies the type of workouts to retrieve. Can be `running`, `walking` or `cycling`|

**Status codes**

| Status code      | Description                                                                                            |
| ---------------- | ------------------------------------------------------------------------------------------------------ |
| 200 OK           | Indicates a successful response.                                                                       |
| 401 Unauthorized | Indicates that the request has not been authenticated. Check the response body for additional details. |

Example response:

```json
{
    "total": {
        "counts": 23,
        "distance": 389.329999999999,
        "duration": 8413.0,
        "pace": 343.5845797100026,
        "speed": 10.58757450872999
    },
    "years": [
        {
            "counts": 5,
            "distance": 15.376999999999999,
            "duration": 5522.0,
            "pace": 340.55805212060034,
            "speed": 10.76021992072195,
            "year": 2016
        }
    ],
    "months": [
        {
            "counts": 2,
            "distance": 2.018,
            "duration": 586.0,
            "pace": 280.943025540275,
            "speed": 12.813986013986014,
            "year": 2016,
            "month": 3
        }
    ]
}
```

### Get all workouts

Returns a list of all workouts of a given type.

**`GET /v1/workouts/list/:type`**

**Parameters**

| Name            | Type    | In     | Required | Description                                                                                                          |
|-----------------|---------|--------|----------|----------------------------------------------------------------------------------------------------------------------|
| `Authorization` | string  | header | Yes      | Specifies the bearer token of the API client.                                                                        |
| `type`          | string  | path   | Yes      | Specifies the type of workouts to retrieve. Can be `running`, `walking` or `cycling`                                 |
| `pageNo`        | integer | query  | No       | Page number                                                                                                          |                                                                         |
| `pageSize`      | integer | query  | No       | Page size                                                                                                            |                                                                         |
| `sortBy`        | string  | query  | No       | Specifies the field to sort the results. Can be `timestamp`, `distance`, `duration`, `notes`, `id`, `pace`, `speed`, `utcOffset`. |                                                                         |
| `sortDir`       | string  | query  | No       | Sort order. Can be `asc` (default) or `desc`                                                                         |                                                                         |

**Status codes**

| Status code      | Description                                                                                            |
| ---------------- | ------------------------------------------------------------------------------------------------------ |
| 200 OK           | Indicates a successful response.                                                                       |
| 401 Unauthorized | Indicates that the request has not been authenticated. Check the response body for additional details. |

Example response:

```json
{
    "content": [
        {
            "distance": 21.999,
            "duration": 8073.0,
            "timestamp": "2022-06-25T04:47:20",
            "notes": null,
            "type": "running",
            "id": 1238,
            "pace": 366.9712259648166,
            "speed": 9.810033444816053,
            "label": null,
            "utcOffset": 1,
            "hasTrajectory": true,
            "trajectory": null
        },
        {
            "distance": 21.271,
            "duration": 7122.0,
            "timestamp": "2022-06-03T08:04:13",
            "notes": null,
            "type": "running",
            "id": 1198,
            "pace": 334.82205820130696,
            "speed": 10.751979780960404,
            "label": null,
            "utcOffset": 1,
            "hasTrajectory": false,
            "trajectory": null
        }
    ],
    "last": false,
    "pageNo": 0,
    "pageSize": 2,
    "totalElements": 20,
    "totalPages": 10
}
```

### Get best results

Returns best results for workouts of a given type.

**`GET /v1/workouts/records/:type`**

**Parameters**

| Name            | Type   | In     | Required | Description                                                                 |
| --------------- |--------| ------ | -------- |-----------------------------------------------------------------------------|
| `Authorization` | string | header | Yes      | Specifies the bearer token of the API client.                               |
| `type`     | string | path  | Yes      | Specifies the type of workouts to retrieve. Can be `running`, `walking` or `cycling`|

**Status codes**

| Status code      | Description                                                                                            |
| ---------------- | ------------------------------------------------------------------------------------------------------ |
| 200 OK           | Indicates a successful response.                                                                       |
| 401 Unauthorized | Indicates that the request has not been authenticated. Check the response body for additional details. |

Example response:

```json
{
    "half_marathon": {
        "distance": 21.271,
        "duration": 7122.0,
        "timestamp": "2022-06-03T08:04:13",
        "notes": null,
        "type": "running",
        "id": 1198,
        "pace": 334.82205820130696,
        "speed": 10.751979780960404,
        "label": null,
        "utcOffset": 1,
        "hasTrajectory": true
    },
    "marathon": null
}
```

### Get workout preview

Get a preview before adding a workout to check if the same or similar workout already exists

**`POST /v1/workouts/preview`**

**Parameters**

| Name            | Type    | In     | Required | Description                                            |
|-----------------|---------|--------|----------|--------------------------------------------------------|
| `Authorization` | string  | header | Yes      | Specifies the bearer token of the API client.          |
| `id`            | integer | path   | Yes      | Specifies the id of the workout to be updated.         |
| `distance`      | float   | body   | Yes      | Distance in km                                         |                   |
| `timestamp`      | string  | body   | Yes      | Time and date of the beginning of the workout          |                   |
| `notes`      | string  | body   | Yes      | Additional notes                                       |                   |
| `type`      | string  | body   | Yes      | Workout type. Can be `running`, `walking` or `cycling` |                   |
| `label`      | object  | body   | Yes      | Label (`label` object or `null`)                       |                   |
| `utcOffset`      | integer | body   | Yes      | Offset of the timezone that the workout took place in  |                   |
| `trajectory`      | geojson | body   | Yes      | Workout trajectory (`geoJSON` object or `null`)        |                   |

**Status codes**

| Status code | Description |
|-----------------|-----------------------------------------------------|
| 200 OK | Indicates a successful response. |
| 400 Bad Request | Indicates that the parameters provided are invalid. |

Example request body:

```json
{
    "distance": 10.0,
    "duration": 3005.0,
    "timestamp": "2020-06-25 23:00:00",
    "notes": null,
    "type": "running",
    "label": null,
    "trajectory": null,
    "hasTrajectory": false,
    "utcOffset": 0
}
```

Example response:

```json
{
    "data": {
        "distance": 10.0,
        "duration": 3005.0,
        "timestamp": "2020-06-25 23:00:00",
        "notes": null,
        "type": "running",
        "label": null,
        "trajectory": null,
        "hasTrajectory": false,
        "utcOffset": 0
    },
    "foundData": [
        {
            "distance": 10.0,
            "duration": 3005.0,
            "timestamp": "2020-06-25 23:00:00",
            "notes": null,
            "type": "running",
            "id": 1000,
            "pace": 300.5,
            "speed": 11.980033277870216,
            "label": null,
            "hasTrajectory": false,
            "trajectory": null,
            "utcOffset": 0
        }
    ]
}
```


### Add new workout

Add a new workout to the database

**`POST /v1/workouts`**


**Parameters**

| Name            | Type    | In     | Required | Description                                            |
|-----------------|---------|--------|----------|--------------------------------------------------------|
| `Authorization` | string  | header | Yes      | Specifies the bearer token of the API client.          |
| `id`            | integer | path   | Yes      | Specifies the id of the workout to be updated.         |
| `distance`      | float   | body   | Yes      | Distance in km                                         |                   |
| `timestamp`      | string  | body   | Yes      | Time and date of the beginning of the workout          |                   |
| `notes`      | string  | body   | Yes      | Additional notes                                       |                   |
| `type`      | string  | body   | Yes      | Workout type. Can be `running`, `walking` or `cycling` |                   |
| `label`      | object  | body   | Yes      | Label (`label` object or `null`)                       |                   |
| `utcOffset`      | integer | body   | Yes      | Offset of the timezone that the workout took place in  |                   |
| `trajectory`      | geojson | body   | Yes      | Workout trajectory (`geoJSON` object or `null`)        |                   |

**Status codes**

| Status code     | Description                                         |
|-----------------|-----------------------------------------------------|
| 201 Created     | Indicates a successful creation of a new workout.   |
| 400 Bad Request | Indicates that the parameters provided are invalid. |

Example request body:

```json
{
    "distance": 10.0,
    "duration": 3005.0,
    "timestamp": "2020-06-25 23:00:00",
    "notes": null,
    "type": "running",
    "label": null,
    "trajectory": null,
    "hasTrajectory": false,
    "utcOffset": 0
}
```

Example response:

```json
{
    "distance": 10.0,
    "duration": 3005.0,
    "timestamp": "2020-06-25 23:00:00",
    "notes": null,
    "type": "running",
    "id": 1000,
    "pace": 300.5,
    "speed": 11.980033277870216,
    "label": null,
    "trajectory": null,
    "hasTrajectory": false,
    "utcOffset": 0
}
```

### Edit workout

Update details of an existing workout

**`PUT /v1/workouts/:id`**

**Parameters**

| Name            | Type    | In     | Required | Description                                            |
|-----------------|---------|--------|----------|--------------------------------------------------------|
| `Authorization` | string  | header | Yes      | Specifies the bearer token of the API client.          |
| `id`            | integer | path   | Yes      | Specifies the id of the workout to be updated.         |
| `distance`      | float   | body   | Yes      | Distance in km                                         |                   |
| `timestamp`      | string  | body   | Yes      | Time and date of the beginning of the workout          |                   |
| `notes`      | string  | body   | Yes      | Additional notes                                       |                   |
| `type`      | string  | body   | Yes      | Workout type. Can be `running`, `walking` or `cycling` |                   |
| `label`      | object  | body   | Yes      | Label (`label` object or `null`)                       |                   |
| `utcOffset`      | integer | body   | Yes      | Offset of the timezone that the workout took place in  |                   |
| `trajectory`      | geojson | body   | Yes      | Workout trajectory (`geoJSON` object or `null`)        |                   |

**Status codes**

| Status code | Description |
|-----------------|-----------------------------------------------------|
| 200 OK | Indicates a successful response. |
| 400 Bad Request | Indicates that the parameters provided are invalid. |

Example request body:

```json
{
    "distance": 10.0,
    "duration": 3005.0,
    "timestamp": "2020-06-25 23:00:00",
    "notes": null,
    "type": "running",
    "label": null,
    "trajectory": null,
    "hasTrajectory": false,
    "utcOffset": 0
}
```

Example response:

```json
{
    "distance": 10.0,
    "duration": 3005.0,
    "timestamp": "2020-06-25 23:00:00",
    "notes": null,
    "type": "running",
    "id": 1000,
    "pace": 300.5,
    "speed": 11.980033277870216,
    "label": null,
    "trajectory": null,
    "hasTrajectory": false,
    "utcOffset": 0
}
```

### Delete workout

Delete a workout

**`DELETE /v1/workouts/:id`**

**Parameters**

| Name        | Type    | In    | Required | Description                                    |
|-------------|---------|-------|----------|------------------------------------------------|
| `Authorization` | string | header | Yes      | Specifies the bearer token of the API client.  |
| `id`        | integer | path  | Yes      | Specifies the id of the workout to be deleted. |

**Status codes**

| Status code | Description                                         |
|-----------------|-----------------------------------------------------|
| 200 OK | Indicates a successful deletion.                    |
| 400 Bad Request | Indicates that the parameters provided are invalid. |

## Labels

### Get labels

**`GET /v1/labels`**

Returns a list of all labels associated with workouts of this user.

**Parameters**

| Name            | Type   | In     | Required | Description                                   |
| --------------- | ------ | ------ | -------- | --------------------------------------------- |
| `Authorization` | string | header | Yes      | Specifies the bearer token of the API client. |

**Status codes**

| Status code      | Description                                                                                            |
| ---------------- | ------------------------------------------------------------------------------------------------------ |
| 200 OK           | Indicates a successful response.                                                                       |
| 401 Unauthorized | Indicates that the request has not been authenticated. Check the response body for additional details. |

Example response:

```json
[
    {
        "color": "#0c56c9",
        "value": "Half Marathon"
    },
    {
        "color": "#0c6b74",
        "value": "Epping Forest"
    }
]
```

## Users

### Get user details

Returns details of the current user.

**`GET /v1/users/current`**

**Parameters**

| Name            | Type   | In     | Required | Description                                   |
| --------------- | ------ | ------ | -------- | --------------------------------------------- |
| `Authorization` | string | header | Yes      | Specifies the bearer token of the API client. |

**Status codes**

| Status code      | Description                                                                                            |
| ---------------- | ------------------------------------------------------------------------------------------------------ |
| 200 OK           | Indicates a successful response.                                                                       |
| 401 Unauthorized | Indicates that the request has not been authenticated. Check the response body for additional details. |

Example response:

```json
{
    "email": "jon@example.com",
    "name": "Jon",
    "verified": false,
    "createdAt": "2023-02-19T09:41:38.000+00:00",
    "updatedAt": "2023-02-19T09:42:02.000+00:00",
    "lastLogin": "2023-02-19T09:42:02.000+00:00",
    "loginCount": 17
}
```

### Delete a user

**`DELETE /v1/users/current`**

Delete the current user and all data associated with it.

**Parameters**

| Name            | Type   | In     | Required | Description                         |
| --------------- | ------ | ------ | -------- | ----------------------------------- |
| `Authorization` | string | header | Yes      | The bearer token of the API client. |

**Status codes**

| Status code      | Description                                                                                            |
|------------------| ------------------------------------------------------------------------------------------------------ |
| 200 No Content   | Indicates that the order has been deleted successfully.                                                |
| 401 Unauthorized | Indicates that the request has not been authenticated. Check the response body for additional details. |
