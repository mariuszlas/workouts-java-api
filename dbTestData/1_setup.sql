DROP TABLE IF EXISTS `users`;

CREATE TABLE
    IF NOT EXISTS `users` (
        `id` bigint NOT NULL AUTO_INCREMENT,
        `created_at` datetime NOT NULL,
        `email` varchar(255) NOT NULL,
        `last_login` datetime DEFAULT NULL,
        `logins_count` bigint NOT NULL,
        `name` varchar(255) NOT NULL,
        `password` varchar(255) NOT NULL,
        `updated_at` datetime DEFAULT NULL,
        `verified` bit (1) NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `UK_6dotkott2kjsp8vw4d0m25fb7` (`email`)
    );

DROP TABLE IF EXISTS `roles`;

CREATE TABLE
    IF NOT EXISTS `roles` (
        `id` bigint NOT NULL AUTO_INCREMENT,
        `name` varchar(255) DEFAULT NULL,
        PRIMARY KEY (`id`)
    );

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE
    IF NOT EXISTS `user_roles` (
        `user_id` bigint NOT NULL,
        `role_id` bigint NOT NULL,
        PRIMARY KEY (`user_id`, `role_id`),
        KEY `FKh8ciramu9cc9q3qcqiv4ue8a6` (`role_id`),
        CONSTRAINT `FKh8ciramu9cc9q3qcqiv4ue8a6` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
        CONSTRAINT `FKhfh9dx7w3ubf1co1vdev94g3f` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
    );

DROP TABLE IF EXISTS `geolocation`;

CREATE TABLE
    IF NOT EXISTS `geolocation` (
        `id` bigint NOT NULL AUTO_INCREMENT,
        `geometry` geometry NOT NULL,
        PRIMARY KEY (`id`)
    );

DROP TABLE IF EXISTS `labels`;

CREATE TABLE
    IF NOT EXISTS `labels` (
        `id` bigint NOT NULL AUTO_INCREMENT,
        `color` varchar(255) NOT NULL,
        `value` varchar(255) NOT NULL,
        `user_id` bigint NOT NULL,
        PRIMARY KEY (`id`),
        KEY `FKjwe9grxkwp2ojnomdy3y1sq02` (`user_id`),
        CONSTRAINT `FKjwe9grxkwp2ojnomdy3y1sq02` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
    );

DROP TABLE IF EXISTS `workouts`;

CREATE TABLE
    IF NOT EXISTS `workouts` (
        `id` bigint NOT NULL AUTO_INCREMENT,
        `distance` double NOT NULL,
        `duration` double NOT NULL,
        `notes` varchar(255) DEFAULT NULL,
        `pace` double NOT NULL,
        `speed` double NOT NULL,
        `timestamp` datetime NOT NULL,
        `type` varchar(255) DEFAULT NULL,
        `utc_offset` bigint NOT NULL,
        `geolocation_id` bigint DEFAULT NULL,
        `label_id` bigint DEFAULT NULL,
        `user_id` bigint NOT NULL,
        PRIMARY KEY (`id`),
        KEY `FKh5mt1smc25mnq4niaakkwt9ft` (`geolocation_id`),
        KEY `FK1f3vcxq69yk2eri49k9x5ux76` (`label_id`),
        KEY `FKpf8ql3wbw2drijbk1ugfvki3d` (`user_id`),
        CONSTRAINT `FK1f3vcxq69yk2eri49k9x5ux76` FOREIGN KEY (`label_id`) REFERENCES `labels` (`id`),
        CONSTRAINT `FKh5mt1smc25mnq4niaakkwt9ft` FOREIGN KEY (`geolocation_id`) REFERENCES `geolocation` (`id`),
        CONSTRAINT `FKpf8ql3wbw2drijbk1ugfvki3d` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
    );