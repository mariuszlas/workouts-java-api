package app.workouts;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SecurityScheme(name = "jwt", scheme = "bearer", type = SecuritySchemeType.HTTP)
@SpringBootApplication
public class WorkoutsApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorkoutsApplication.class, args);
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
