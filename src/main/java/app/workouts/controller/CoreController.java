package app.workouts.controller;

import app.workouts.dto.response.ApiStatus;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "status")
@RestController
@RequestMapping("/")
public class CoreController {
    @GetMapping()
    public ResponseEntity<ApiStatus> core() {
        return ResponseEntity.ok(new ApiStatus());
    }
}
