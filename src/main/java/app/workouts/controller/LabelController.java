package app.workouts.controller;

import app.workouts.dto.LabelDto;
import app.workouts.service.impl.LabelServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "labels")
@SecurityRequirement(name = "jwt")
@RestController
@RequestMapping("/v1/labels")
public class LabelController {
    @Autowired
    LabelServiceImpl labelService;

    @Operation(summary = "Get all labels")
    @GetMapping()
    public ResponseEntity<List<LabelDto>> getAllLabels() {
        List<LabelDto> labels = labelService.getAllLabels();
        return ResponseEntity.ok(labels);
    }
}
