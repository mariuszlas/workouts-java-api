package app.workouts.controller;

import app.workouts.dto.payload.NewUser;
import app.workouts.dto.response.UserDto;
import app.workouts.service.impl.UserServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "users")
@SecurityRequirement(name = "jwt")
@RestController
@RequestMapping("/v1/users")
public class UserController {
    @Autowired
    UserServiceImpl userService;

    @Operation(summary = "Get user details")
    @GetMapping("/current")
    public ResponseEntity<UserDto> getCurrentUser() {
        return ResponseEntity.ok(userService.getCurrentUser());
    }

    @Operation(summary = "Report user login")
    @GetMapping("/current/report-login")
    public ResponseEntity<String> reportUserLogin() {
        return ResponseEntity.ok(userService.reportUserLogin());
    }

    @Operation(summary = "Create new user")
    @PostMapping()
    public ResponseEntity<UserDto> createNewUser(@Valid @RequestBody NewUser newUser) {
        return ResponseEntity.ok(userService.createNewUser(newUser));
    }

    @Operation(summary = "Delete user")
    @DeleteMapping("/current")
    public ResponseEntity<String> deleteCurrentUser() {
        userService.deleteCurrentUser();
        return ResponseEntity.ok("User was successfully deleted");
    }
}
