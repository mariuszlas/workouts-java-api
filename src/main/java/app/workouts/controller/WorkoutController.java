package app.workouts.controller;

import app.workouts.dto.payload.WorkoutUpload;
import app.workouts.dto.response.*;
import app.workouts.utils.WorkoutType;
import app.workouts.service.impl.WorkoutServiceImpl;
import app.workouts.utils.SortFields;
import app.workouts.utils.SortOrder;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@Tag(name = "workouts")
@SecurityRequirement(name = "jwt")
@RestController
@RequestMapping("/v1/workouts")
public class WorkoutController {
    @Autowired
    WorkoutServiceImpl workoutService;

    @Operation(summary = "Get workouts details by id")
    @GetMapping("/{id}")
    public ResponseEntity<WorkoutDto> getWorkoutById(@PathVariable Long id,
            @RequestParam(value = "geolocation", required = false,
            defaultValue = "false") boolean isGeo) {
        return ResponseEntity.ok(workoutService.getWorkoutById(id, isGeo));
    }

    @Operation(summary = "Get geolocation data for a workout")
    @GetMapping("/{id}/geolocation")
    public ResponseEntity<GeolocationDto> getWorkoutGeolocation(
            @PathVariable Long id) {
        return ResponseEntity.ok(workoutService.getWorkoutGeolocationById(id));
    }

    @Operation(summary = "Get list of all workouts for an activity")
    @GetMapping("/list/{type}")
    public ResponseEntity<PagedWorkouts> getWorkoutsByType(
            @PathVariable WorkoutType type,
            @RequestParam(value = "pageNo", required = false) Integer pageNo,
            @RequestParam(value = "pageSize",
                    required = false) Integer pageSize,
            @RequestParam(value = "sortBy", defaultValue = "timestamp",
                    required = false) SortFields sortBy,
            @Valid @RequestParam(value = "sortDir", defaultValue = "asc",
                    required = false) SortOrder sortDir
    ) {
        return ResponseEntity.ok(workoutService.getWorkoutsByType(
                type, pageNo, pageSize, sortBy, sortDir));
    }

    @Operation(summary = "Get best workouts results for an activity")
    @GetMapping("records/{type}")
    public ResponseEntity<HashMap<String, WorkoutDto>>
    getWorkoutRecordsByType(@PathVariable WorkoutType type) {
        return ResponseEntity.ok(workoutService.getWorkoutRecordsByType(type));
    }

    @Operation(summary = "Get yearly and monthly statistics for workouts by " +
            "activity")
    @GetMapping("dashboard/{type}")
    public ResponseEntity<Dashboard> getDashboard(
            @PathVariable WorkoutType type) {
        return ResponseEntity.ok(workoutService.getDashboard(type));
    }

    @Operation(summary = "Add a new workout")
    @PostMapping()
    public ResponseEntity<WorkoutDto> createWorkout(
            @Valid @RequestBody WorkoutUpload workoutUpload) {
        WorkoutDto workout = workoutService.createWorkout(workoutUpload);
        return new ResponseEntity<>(workout, HttpStatus.CREATED);
    }

    @Operation(summary = "Check if the workout already exists")
    @PostMapping("/preview")
    public ResponseEntity<WorkoutPreview> previewWorkout(
            @Valid @RequestBody WorkoutUpload workout) {
        return ResponseEntity.ok(workoutService.getPreview(workout));
    }

    @Operation(summary = "Update a workout")
    @PutMapping("/{id}")
    public ResponseEntity<WorkoutDto> updateWorkout(@PathVariable Long id,
                                                    @Valid @RequestBody WorkoutUpload workout) {
        return ResponseEntity.ok(workoutService.updateWorkout(id, workout));
    }

    @Operation(summary = "Delete a workout")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteWorkout(@PathVariable Long id) {
        workoutService.deleteWorkoutById(id);
        return ResponseEntity.ok("OK");
    }
}
