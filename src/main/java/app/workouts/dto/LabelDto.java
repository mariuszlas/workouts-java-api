package app.workouts.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LabelDto {
    @NotEmpty
    private String value;

    @NotEmpty
    private String color;

    @Override
    public String toString() {
        return "LabelDto{" +
                "value='" + value + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
