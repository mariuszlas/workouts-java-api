package app.workouts.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Month extends Year {
    private Integer month;

    public Month(Integer year, Integer month, Long counts, Double distance,
                 Double duration, Double pace, Double speed) {
        super(year, counts, distance, duration, pace, speed);
        this.month = month;
    }
}
