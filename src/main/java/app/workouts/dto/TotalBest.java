package app.workouts.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TotalBest {
    private Long counts;

    private Double distance;

    private Double duration;

    private Double pace;

    private Double speed;

    @Override
    public String toString() {
        return "TotalBest{" +
                "counts=" + counts +
                ", distance=" + distance +
                ", duration=" + duration +
                ", pace=" + pace +
                ", speed=" + speed +
                '}';
    }
}
