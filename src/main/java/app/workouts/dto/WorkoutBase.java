package app.workouts.dto;

import app.workouts.utils.WorkoutType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class WorkoutBase {
    @NotNull
    @Min(0)
    private double distance;

    @NotNull
    @Min(0)
    private double duration;

    @NotNull
    private Date timestamp;

    @NotNull
    private long utcOffset;

    @Size(max = 500)
    private String notes;

    @NotNull
    @Enumerated(EnumType.STRING)
    private WorkoutType type;
}
