package app.workouts.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Year extends TotalBest {
    private final Integer year;

    public Year(Integer year, Long counts, Double distance,
                Double duration, Double pace, Double speed) {
        super(counts, distance, duration, pace, speed);
        this.year = year;
    }
}
