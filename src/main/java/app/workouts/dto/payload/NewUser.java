package app.workouts.dto.payload;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class NewUser {
        @Email @NotEmpty @Size(max = 120, message = "cannot be longer than " +
                "120 characters")
        private String email;

        @NotEmpty @Size(max = 120, message = "cannot be longer than 120 " +
                "characters")
        private String username;
}
