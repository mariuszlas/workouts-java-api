package app.workouts.dto.payload;

import app.workouts.dto.LabelDto;
import app.workouts.dto.WorkoutBase;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class WorkoutUpload extends WorkoutBase {
    @Valid
    private LabelDto label;
    @Valid
    private List<List<Double>> geolocation;
}