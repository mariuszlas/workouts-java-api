package app.workouts.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class ApiStatus {
    private final String name = "Workouts API";
    private final String status = "UP";
}
