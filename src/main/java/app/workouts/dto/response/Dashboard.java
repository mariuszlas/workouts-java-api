package app.workouts.dto.response;

import app.workouts.dto.Month;
import app.workouts.dto.TotalBest;
import app.workouts.dto.Year;

import java.util.List;

public record Dashboard(TotalBest total, List<Year> years, List<Month> months) {
}
