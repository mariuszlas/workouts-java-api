package app.workouts.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@Getter
public class ErrorDetails {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private final Date timestamp;
    private final int status;
    private final HttpStatus error;
    private final String message;
    private final String path;

    public ErrorDetails(HttpStatus error, Exception exception, String path) {
        this(error, exception.getMessage(), path);
    }

    public ErrorDetails(HttpStatus error, Exception exception,
                        WebRequest request) {
        this(error, exception.getMessage(), request);
    }

    public ErrorDetails(HttpStatus error, String message, WebRequest request) {
        this(error, message, request.getDescription(false).split("=")[1]);
    }

    public ErrorDetails(HttpStatus error, String message, String path) {
        this.timestamp = new Date();
        this.status = error.value();
        this.error = error;
        this.message = message;
        this.path = path;
    }
}
