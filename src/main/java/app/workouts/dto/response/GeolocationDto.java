package app.workouts.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class GeolocationDto {
    private Long workoutId;

    private LineStringGeoJson trajectory;
}
