package app.workouts.dto.response;

import lombok.Getter;

import java.util.List;

@Getter
public class LineStringGeoJson {
    private final String type = "Feature";
    private final String properties = null;
    private final Geometry geometry;

    public LineStringGeoJson(List<List<Double>> coordinates,
                             String geometryType) {
        this.geometry = new Geometry(geometryType, coordinates);
    }
}

record Geometry(String type, List<List<Double>> coordinates) {
}
