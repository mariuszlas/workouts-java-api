package app.workouts.dto.response;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

@Getter
public class ValidationErrorDetails extends ErrorDetails {
    private final Map<String, String> schema;

    public ValidationErrorDetails(WebRequest request,
                                  Map<String, String> errors) {
        super(HttpStatus.BAD_REQUEST, "Validation failed", request);
        this.schema = errors;
    }
}
