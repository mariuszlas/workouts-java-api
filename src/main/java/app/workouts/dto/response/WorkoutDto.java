package app.workouts.dto.response;

import app.workouts.dto.LabelDto;
import app.workouts.dto.WorkoutBase;
import app.workouts.model.Label;
import app.workouts.utils.Helpers;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
public class WorkoutDto extends WorkoutBase {
    @Setter
    private Long id;

    @Setter
    private Double pace;

    @Setter
    private Double speed;

    private LabelDto label;

    @Setter
    private LineStringGeoJson trajectory;

    @Setter
    private boolean hasTrajectory;

    public void setLabel(Label label) {
        this.label = Helpers.generateLabel(label);
    }
}
