package app.workouts.dto.response;

import app.workouts.dto.payload.WorkoutUpload;

import java.util.List;

public record WorkoutPreview(WorkoutUpload data, List<WorkoutDto> foundData) {
}
