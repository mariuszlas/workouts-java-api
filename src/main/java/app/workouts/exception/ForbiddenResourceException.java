package app.workouts.exception;

public class ForbiddenResourceException extends RuntimeException {
    public ForbiddenResourceException(String email) {
        super(String.format("Resource forbidden for user: '%s'", email));
    }
}
