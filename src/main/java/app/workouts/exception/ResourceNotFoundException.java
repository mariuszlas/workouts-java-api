package app.workouts.exception;

import lombok.Getter;

@Getter
public class ResourceNotFoundException extends RuntimeException {
    private final String resourceName;
    private final String fieldName;
    private final String fieldValue;

    public ResourceNotFoundException(String resourceName,
                                     String fieldName, Long fieldValue) {
        this(resourceName, fieldName, fieldValue.toString());
    }

    public ResourceNotFoundException(String resourceName,
                                     String fieldName, String fieldValue) {
        super(String.format("%s with %s '%s' was not found",
                resourceName, fieldName, fieldValue));
        this.resourceName = resourceName;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }
}
