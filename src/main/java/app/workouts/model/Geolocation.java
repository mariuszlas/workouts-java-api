package app.workouts.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.locationtech.jts.geom.LineString;

@Entity
@Table(name = "geolocation")
@NoArgsConstructor
@Getter
@Setter
public class Geolocation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LineString geometry;

    public Geolocation(LineString geometry) {
        this.geometry = geometry;
    }

    @Override
    public String toString() {
        return "Geolocation{" +
                "id=" + id +
                ", geometry=" + geometry.toString() +
                '}';
    }
}
