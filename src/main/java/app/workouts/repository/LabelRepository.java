package app.workouts.repository;

import app.workouts.model.Label;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface LabelRepository extends JpaRepository<Label, Long> {
    List<Label> findAllByUserId(Long userId);

    Optional<Label> findByValueAndColorAndUserId(
            String value, String color, Long userId);
}
