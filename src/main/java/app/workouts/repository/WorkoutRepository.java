package app.workouts.repository;

import app.workouts.dto.Month;
import app.workouts.dto.TotalBest;
import app.workouts.dto.Year;
import app.workouts.model.User;
import app.workouts.model.Workout;
import app.workouts.utils.WorkoutType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.NonNull;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface WorkoutRepository extends JpaRepository<Workout, Long> {
    Optional<Workout> findById(@NonNull Long id);

    Page<Workout> findAllByUserIdAndType(Long userId, WorkoutType type, Pageable pageable);

    List<Workout> findAllByUserIdAndTypeAndTimestampGreaterThanEqualAndTimestampLessThan(
            Long userId, WorkoutType type, Date startDate, Date endDate);

    @Query("""
            SELECT NEW app.workouts.dto.TotalBest(
                COUNT(w.id) AS counts,
                SUM(w.distance) AS distance,
                SUM(w.duration) AS duration,
                AVG(w.pace) AS pace,
                AVG(w.speed) AS speed)
            FROM Workout w
            WHERE w.type = :type AND w.user = :user
            """)
    TotalBest getTotalBest(User user, WorkoutType type);

    @Query("""
            SELECT NEW app.workouts.dto.Year(
                EXTRACT (year FROM w.timestamp) AS year,
                COUNT(w.id) AS counts,
                SUM(w.distance) AS distance,
                SUM(w.duration) AS duration,
                AVG(w.pace) AS pace,
                AVG(w.speed) AS speed)
            FROM Workout w
            WHERE w.type = :type AND w.user = :user
            GROUP BY EXTRACT(year FROM w.timestamp)
            ORDER BY EXTRACT(year FROM w.timestamp)
            """)
    List<Year> getYears(User user, WorkoutType type);

    @Query("""
            SELECT NEW app.workouts.dto.Month(
                EXTRACT (year FROM w.timestamp) AS year,
                EXTRACT (month FROM w.timestamp) AS month,
                COUNT(w.id) AS counts,
                SUM(w.distance) AS distance,
                SUM(w.duration) AS duration,
                AVG(w.pace) AS pace,
                AVG(w.speed) AS speed)
            FROM Workout w
            WHERE w.type = :type AND w.user = :user
            GROUP BY
                EXTRACT (year FROM w.timestamp),
                EXTRACT (month FROM w.timestamp)
            ORDER BY
                EXTRACT (year FROM w.timestamp),
                EXTRACT (month FROM w.timestamp)
            """)
    List<Month> getMonths(User user, WorkoutType type);

    @Query("""
            SELECT w FROM Workout w
            WHERE w.pace = (
                SELECT MIN(w.pace) FROM Workout w
                    WHERE w.distance
                        BETWEEN :min AND :max
                        AND w.type = :type
                        AND w.user = :user
            )
            """)
    Optional<Workout> getBestResult(
            double min, double max, User user, WorkoutType type);
}
