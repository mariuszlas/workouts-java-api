package app.workouts.service;

import app.workouts.model.Label;
import app.workouts.model.User;
import app.workouts.dto.LabelDto;

import java.util.List;

public interface LabelService {
    List<LabelDto> getAllLabels();

    Label getOrCreateLabel(LabelDto labelDto, User user);
}
