package app.workouts.service;

import app.workouts.dto.payload.NewUser;
import app.workouts.dto.response.UserDto;

public interface UserService {
    UserDto getCurrentUser();

    UserDto createNewUser(NewUser newUser);

    String reportUserLogin();

    void deleteCurrentUser();
}
