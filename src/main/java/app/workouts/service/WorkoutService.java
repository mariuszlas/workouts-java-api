package app.workouts.service;

import app.workouts.dto.payload.WorkoutUpload;
import app.workouts.dto.response.*;
import app.workouts.utils.SortFields;
import app.workouts.utils.SortOrder;
import app.workouts.utils.WorkoutType;

import java.util.HashMap;

public interface WorkoutService {
    WorkoutDto getWorkoutById(Long id, boolean isGeo);

    GeolocationDto getWorkoutGeolocationById(Long id);

    PagedWorkouts getWorkoutsByType(WorkoutType type, Integer pageNo,
                                    Integer pageSize, SortFields sortBy,
                                    SortOrder sortDir);

    HashMap<String, WorkoutDto> getWorkoutRecordsByType(
            WorkoutType type);

    Dashboard getDashboard(WorkoutType type);

    WorkoutDto createWorkout(WorkoutUpload workout);

    WorkoutDto updateWorkout(Long id, WorkoutUpload workout);

    void deleteWorkoutById(Long id);

    WorkoutPreview getPreview(WorkoutUpload workout);
}
