package app.workouts.service.impl;

import app.workouts.dto.LabelDto;
import app.workouts.model.Label;
import app.workouts.model.User;
import app.workouts.repository.LabelRepository;
import app.workouts.service.LabelService;
import app.workouts.utils.DtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LabelServiceImpl implements LabelService {
    @Autowired
    LabelRepository labelRepository;

    @Autowired
    UserServiceImpl userService;

    @Autowired
    DtoMapper dtoMapper;

    @Override
    public List<LabelDto> getAllLabels() {
        User user = userService.getCurrentUserEntity();

        return labelRepository
                .findAllByUserId(user.getId())
                .stream()
                .map(label -> dtoMapper.toLabelDto(label))
                .collect(Collectors.toList());
    }

    @Override
    public Label getOrCreateLabel(LabelDto labelDto, User user) {
        if (labelDto == null) {
            return null;
        }

        return labelRepository
                .findByValueAndColorAndUserId(
                        labelDto.getValue(), labelDto.getColor(), user.getId())
                .orElseGet(() -> {
                    Label newLabel = new Label();
                    newLabel.setValue(labelDto.getValue());
                    newLabel.setColor(labelDto.getColor());
                    newLabel.setUser(user);
                    return newLabel;
                });
    }
}
