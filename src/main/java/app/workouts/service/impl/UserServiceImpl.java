package app.workouts.service.impl;

import app.workouts.dto.payload.NewUser;
import app.workouts.dto.response.UserDto;
import app.workouts.model.User;
import app.workouts.repository.UserRepository;
import app.workouts.service.UserService;
import app.workouts.utils.DtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    DtoMapper dtoMapper;

    @Override
    public UserDto getCurrentUser() {
        return dtoMapper.toUserDto(getCurrentUserEntity());
    }

    @Override
    public UserDto createNewUser(NewUser newUser) {
        User user = new User();
        user.setEmail(newUser.getEmail());
        user.setUsername(newUser.getUsername());

        return dtoMapper.toUserDto(userRepository.save(user));
    }

    @Override
    public String reportUserLogin() {
        User user = getCurrentUserEntity();
        user.setLastLogin(new Date());
        user.setLoginCount(user.getLoginCount() + 1);
        userRepository.save(user);

        return "User login was successfully reported";
    }

    @Override
    public void deleteCurrentUser() {
        userRepository.deleteById(getCurrentUserEntity().getId());
    }

    public User getCurrentUserEntity() {
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        String username = authentication.getName();

        return userRepository
                .findByUsername(username)
                .orElseThrow(
                        () -> new UsernameNotFoundException(
                                String.format("User not found: %s", username)));
    }
}
