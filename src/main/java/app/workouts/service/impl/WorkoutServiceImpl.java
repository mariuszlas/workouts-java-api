package app.workouts.service.impl;

import app.workouts.dto.Month;
import app.workouts.dto.TotalBest;
import app.workouts.dto.Year;
import app.workouts.dto.payload.WorkoutUpload;
import app.workouts.dto.response.*;
import app.workouts.exception.ForbiddenResourceException;
import app.workouts.exception.ResourceNotFoundException;
import app.workouts.model.Geolocation;
import app.workouts.model.Label;
import app.workouts.model.User;
import app.workouts.model.Workout;
import app.workouts.repository.WorkoutRepository;
import app.workouts.service.WorkoutService;
import app.workouts.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WorkoutServiceImpl implements WorkoutService {
    @Autowired
    WorkoutRepository workoutRepository;

    @Autowired
    UserServiceImpl userService;

    @Autowired
    LabelServiceImpl labelService;

    @Autowired
    DtoMapper dtoMapper;

    @Override
    public WorkoutDto getWorkoutById(Long id, boolean isGeo) {
        return dtoMapper.toWorkoutDto(getDbWorkoutById(id), isGeo);
    }

    @Override
    public GeolocationDto getWorkoutGeolocationById(Long id) {
        return dtoMapper.toGeolocationDto(getDbWorkoutById(id));
    }

    @Override
    public PagedWorkouts getWorkoutsByType(WorkoutType type, Integer pageNo,
                                           Integer pageSize, SortFields sortBy,
                                           SortOrder sortDir) {
        Pageable pageable = null;

        if (pageNo != null && pageSize != null) {
            Sort sort = null;

            if (SortOrder.asc == sortDir) {
                sort = Sort.by(sortBy.name()).ascending();
            }
            if (SortOrder.desc == sortDir) {
                sort = Sort.by(sortBy.name()).descending();
            }
            pageable = PageRequest.of(pageNo, pageSize, sort);
        }

        User user = userService.getCurrentUserEntity();
        Page<Workout> workouts = workoutRepository
                .findAllByUserIdAndType(user.getId(), type, pageable);

        return dtoMapper.toPagedWorkoutsDto(workouts);
    }

    @Override
    public HashMap<String, WorkoutDto> getWorkoutRecordsByType(
            WorkoutType type) {
        User user = userService.getCurrentUserEntity();
        HashMap<String, WorkoutDto> bestResults = new HashMap<>();

        Ranges.getRanges(type).forEach(range -> {
            Optional<Workout> workout = getBestResult(
                    range.min(), range.max(), user, type);
            WorkoutDto result = workout
                    .map(item -> dtoMapper.toWorkoutDto(item, false))
                    .orElse(null);
            bestResults.put(range.label(), result);
        });
        return bestResults;
    }

    @Override
    public Dashboard getDashboard(WorkoutType type) {
        User user = userService.getCurrentUserEntity();
        TotalBest totalBest = getTotalBest(user, type);
        List<Year> years = getYears(user, type);
        List<Month> months = getMonths(user, type);
        return new Dashboard(totalBest, years, months);
    }

    @Override
    public WorkoutPreview getPreview(WorkoutUpload workout) {
        User user = userService.getCurrentUserEntity();

        Date timestamp = workout.getTimestamp();
        Date startDate = Helpers.getDayHourTimestamp(timestamp, 0);
        Date endDate = Helpers.getDayHourTimestamp(timestamp, 24);

        List<WorkoutDto> workouts = workoutRepository
                .findAllByUserIdAndTypeAndTimestampGreaterThanEqualAndTimestampLessThan(
                        user.getId(), workout.getType(), startDate, endDate)
                .stream()
                .map(item -> dtoMapper.toWorkoutDto(item))
                .collect(Collectors.toList());

        return new WorkoutPreview(workout, workouts);
    }

    @Override
    public WorkoutDto createWorkout(WorkoutUpload workoutDto) {
        User user = userService.getCurrentUserEntity();
        Label label = labelService
                .getOrCreateLabel(workoutDto.getLabel(), user);
        Geolocation geolocation = Helpers.buildGeolocation(workoutDto);

        Workout workout = dtoMapper.toWorkoutEntity(workoutDto);
        workout.setUser(user);
        workout.setLabel(label);
        workout.setGeolocation(geolocation);
        Workout newWorkout = workoutRepository.save(workout);

        return dtoMapper.toWorkoutDto(newWorkout);
    }

    @Override
    public WorkoutDto updateWorkout(Long id, WorkoutUpload workoutUpload) {
        User user = userService.getCurrentUserEntity();
        Label label = labelService
                .getOrCreateLabel(workoutUpload.getLabel(), user);

        Workout workout = getDbWorkoutById(id);
        dtoMapper.mapWorkoutDtoToEntityAttributes(workout, workoutUpload);
        workout.setLabel(label);

        Workout updatedWorkout = workoutRepository.save(workout);
        return dtoMapper.toWorkoutDto(updatedWorkout);
    }

    @Override
    public void deleteWorkoutById(Long id) {
        workoutRepository.deleteById(id);
    }

    private TotalBest getTotalBest(User user, WorkoutType type) {
        return workoutRepository.getTotalBest(user, type);
    }

    private List<Year> getYears(User user, WorkoutType type) {
        return workoutRepository.getYears(user, type);
    }

    private List<Month> getMonths(User user, WorkoutType type) {
        return workoutRepository.getMonths(user, type);
    }

    private Optional<Workout> getBestResult(
            double min, double max, User user, WorkoutType type) {
        return workoutRepository.getBestResult(min, max, user, type);
    }

    private Workout getDbWorkoutById(Long id) {
        User user = userService.getCurrentUserEntity();

        Workout workout = workoutRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Workout", "id", id));

        if (!workout.getUser().getId().equals(user.getId())) {
            throw new ForbiddenResourceException(user.getEmail());
        }

        return workout;
    }
}
