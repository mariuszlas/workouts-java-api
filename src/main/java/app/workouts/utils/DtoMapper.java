package app.workouts.utils;

import app.workouts.dto.LabelDto;
import app.workouts.dto.payload.WorkoutUpload;
import app.workouts.dto.response.*;
import app.workouts.model.Label;
import app.workouts.model.User;
import app.workouts.model.Workout;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DtoMapper {
    @Autowired
    ModelMapper modelMapper;

    public UserDto toUserDto(User user) {
        return modelMapper.map(user, UserDto.class);
    }

    public LabelDto toLabelDto(Label label) {
        return modelMapper.map(label, LabelDto.class);
    }

    public WorkoutDto toWorkoutDto(Workout workout) {
        return toWorkoutDto(workout, true);
    }

    public WorkoutDto toWorkoutDto(Workout workout, boolean isGeo) {
        WorkoutDto workoutDto = modelMapper
                .map(workout, WorkoutDto.class);
        workoutDto.setHasTrajectory(workout.getGeolocation() != null);

        if (isGeo) {
            workoutDto.setTrajectory(Helpers.formatTrajectory(workout));
        }

        return workoutDto;
    }

    public void mapWorkoutDtoToEntityAttributes(
            Workout workout, WorkoutUpload workoutUpload) {
        modelMapper.map(workoutUpload, workout);
        workout.setPace(Helpers.calculatePace(workout));
        workout.setSpeed(Helpers.calculateSpeed(workout));
    }

    public Workout toWorkoutEntity(WorkoutUpload workoutUpload) {
        Workout workout = new Workout();
        mapWorkoutDtoToEntityAttributes(workout, workoutUpload);
        return workout;
    }

    public PagedWorkouts toPagedWorkoutsDto(Page<Workout> workouts) {
        List<WorkoutDto> workoutDtoItems = workouts
                .getContent()
                .stream()
                .map(item -> toWorkoutDto(item, false))
                .toList();

        PagedWorkouts pagedWorkouts = modelMapper
                .map(workouts, PagedWorkouts.class);
        pagedWorkouts.setContent(workoutDtoItems);

        return pagedWorkouts;
    }

    public GeolocationDto toGeolocationDto(Workout workout) {
        GeolocationDto geolocationDto = new GeolocationDto();
        geolocationDto.setWorkoutId(workout.getId());
        geolocationDto.setTrajectory(Helpers.formatTrajectory(workout));
        return geolocationDto;
    }
}
