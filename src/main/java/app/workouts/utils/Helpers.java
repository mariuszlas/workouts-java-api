package app.workouts.utils;

import app.workouts.dto.LabelDto;
import app.workouts.dto.payload.WorkoutUpload;
import app.workouts.dto.response.LineStringGeoJson;
import app.workouts.model.Geolocation;
import app.workouts.model.Label;
import app.workouts.model.Workout;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.PrecisionModel;

import java.util.*;

public class Helpers {
    private static final int SEC_IN_MIN = 3600;

    public static Double calculatePace(Workout workout) {
        return workout.getDuration() / workout.getDistance();
    }

    public static Double calculateSpeed(Workout workout) {
        return workout.getDistance() / (workout.getDuration() / SEC_IN_MIN);
    }

    public static LabelDto generateLabel(Label label) {
        return label != null ?
                new LabelDto(label.getValue(), label.getColor()) : null;
    }

    public static Date getDayHourTimestamp(Date timestamp, int h) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(timestamp);

        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, h);

        return calendar.getTime();
    };

    public static Geolocation buildGeolocation(WorkoutUpload workout) {
        List<List<Double>> trajectory = workout.getGeolocation();

        if (trajectory != null) {
            Coordinate[] coordinates = new Coordinate[trajectory.size()];

            for (int i = 0; i < trajectory.size(); i++) {
                coordinates[i] = new Coordinate(
                        trajectory.get(i).get(0), trajectory.get(i).get(1));
            }

            GeometryFactory factory = new GeometryFactory(
                    new PrecisionModel(), 4326);
            LineString lineString = factory.createLineString(coordinates);

            return new Geolocation(lineString);
        }

        return null;
    }

    public static LineStringGeoJson formatTrajectory(Workout workout) {
        Geolocation geolocation = workout.getGeolocation();

        if (geolocation != null) {
            Coordinate[] coordinates =
                    geolocation.getGeometry().getCoordinates();
            String geometryType = geolocation.getGeometry().getGeometryType();

            List<List<Double>> formattedCords =
                    Arrays.stream(coordinates).map(coordinate -> {
                        List<Double> coordPair = new ArrayList<>();
                        coordPair.add(coordinate.x);
                        coordPair.add(coordinate.y);
                        return coordPair;
                    }).toList();

            return new LineStringGeoJson(formattedCords, geometryType);
        }

        return null;
    }
}
