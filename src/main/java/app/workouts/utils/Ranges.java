package app.workouts.utils;

import java.util.ArrayList;
import java.util.List;

public class Ranges {
    private static final String oneK = "one_k";
    private static final String fiveK = "five_k";
    private static final String tenK = "ten_k";
    private static final String hMarathon = "half_marathon";
    private static final String marathon = "marathon";
    private static final String thirtyK = "thirty_k";
    private static final String fiftyK = "fifty_k";
    private static final String hundredK = "hundred_k";
    private static final String hundredFiftyK = "hundred_fifty_k";
    private static final String twoHundredK = "two_hundred_k";

    public record Range(String label, double min, double max) {
    }

    public static List<Range> getRanges(WorkoutType type) {
        List<Range> ranges = new ArrayList<>();

        switch (type) {
            case running -> {
                ranges.add(new Range(oneK, 0.9, 1.1));
                ranges.add(new Range(fiveK, 4.9, 5.1));
                ranges.add(new Range(tenK, 9.9, 10.1));
                ranges.add(new Range(hMarathon, 21.0, 21.4));
                ranges.add(new Range(marathon, 42.0, 42.5));
            }
            case walking -> {
                ranges.add(new Range(tenK, 9.0, 11.0));
                ranges.add(new Range(thirtyK, 28.0, 32.0));
                ranges.add(new Range(fiftyK, 48.0, 52.0));
            }
            case cycling -> {
                ranges.add(new Range(thirtyK, 28.0, 32.0));
                ranges.add(new Range(fiftyK, 48.0, 52.0));
                ranges.add(new Range(hundredK, 97.0, 103.0));
                ranges.add(new Range(hundredFiftyK, 147.0, 153.0));
                ranges.add(new Range(twoHundredK, 196.0, 204.0));
            }
        }
        return ranges;
    }
}

