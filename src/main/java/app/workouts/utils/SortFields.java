package app.workouts.utils;

public enum SortFields {
    distance,
    duration,
    timestamp,
    notes,
    id,
    pace,
    speed,
    utcOffset
}
