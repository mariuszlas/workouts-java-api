package app.workouts.utils;

public enum SortOrder {
    asc,
    desc
}
