package app.workouts.utils;

public enum WorkoutType {
    running,
    walking,
    cycling
}
